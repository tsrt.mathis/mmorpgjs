export default class Character {
    constructor(name,img,health,damage,color,speed, die) {
        this.name = name;
        this.img = img;
        this.health = health;
        this.damage = damage;
        this.color = color;
        this.speed = speed;
        this.die = die;
    }

    isDie(p1,p2)
    {
        p1._die = true;
        return `${p1.name} a succombé face à ${p2.name}!`
    }
};
