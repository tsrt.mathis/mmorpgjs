import Hero from './Hero.js';
import Mob from './Mob.js';
import Item from './Item.js'

// Drake = 4940 HP / 100 dmg
// Gromp : 1800 HP / 70 dmg
// Krug 540
// Red 2300
// Blue 2300
let Mobs = [
  new Mob('Krug',"./img/krug.webp", 50, 35, 'grey', 5,'normal', false),
  new Mob('RedBuff',"./img/redbuff.webp", 50, 66, 'red', 5, 'fire', false),
  new Mob('BlueBuff',"./img/bluebuff.webp", 50, 66, 'blue', 5, 'water', false),
  new Mob('Wraith',"./img/wraith.webp", 1150, 52, 'orange', 5, 'fire', false),
  new Mob('Wolf',"./img/wolf.webp", 1220, 40, 'grey', 5, 'normal', false),
  new Mob('Gromp',"./img/gromp.webp", 100, 7, 'green', 5, 'normal', false),
]
// Filtre les Mobs s'ils sont mort ou pas
let MobNotDie = Mobs.filter((e) => e.die === false)

const Drake = new Mob('Drake', 100, 3, 'red', 5, 'fire', false)


//                      name,health,damage,color,speed, ultimate, passive
const Cabochard = new Hero('Cabochard','./img/cabochard.jpg', 500, 20, 'blue', 300, 70, 1, false);
const Cinkrof = new Hero('Cinkrof','./img/cinkrof.jpg', 800, 10, 'blue', 300, 25, 1, false);
const Saken = new Hero('Saken','./img/saken.jpg', 400, 30, 'blue', 300, 60, 1, false);
const Caliste = new Hero('Caliste','./img/caliste.jpg', 350, 30, 'blue', 300, 50, 1, false);
const Targamas = new Hero('Targamas','./img/targamas.jpg', 650, 5, 'blue', 300, 20, 1, false);
const Kameto = new Hero('Kamet0','./img/kameto.jpg', 1000, 50, 'blue', 300, 40, 1, false);


const Potion = new Item('Potion',5)



// // Premier form
// const inputStartNbMobs = document.querySelector("#quantity");
// document.getElementById("buttonStartNbMobs").addEventListener('click', function() {
//   const nbTurns = inputStartNbMobs.value
//   console.log(nbTurns)
//   const firstForm = document.getElementById("firstForm").classList.add("none")
//   const secondForm = document.getElementById("secondForm").classList.remove("none")


  // Deuxième form
  const buttonChooseCharacter = document.getElementById("buttonChooseCharacter")
  buttonChooseCharacter.addEventListener("click", function(){
    const choix = document.getElementById("sources").value
    document.getElementById("secondForm").classList.add("none")

    // Sélection du perso en fonction du choix du joueur
    let perso;

    switch (choix) {
      case 'Cabochard':
        perso = Cabochard;
        break;
      case 'Cinkrof':
        perso = Cinkrof;
        break;
      case 'Saken':
        perso = Saken;
        break;
      case 'Caliste':
        perso = Caliste;
        break;
      case 'Targamas':
        perso = Targamas;
        break;
      case 'Kameto':
        perso = Kameto
        break
    }

    // Troisième form
    document.getElementById("thirdForm").classList.remove("none")
    const h3AnnouncePerso = document.getElementById("announcePerso").innerHTML = `Vous avez choisi ${perso.name} <br>
    ${perso.name} explore la LEC... <br>`


    document.getElementById("buttonAcceptAnnouncePerso").addEventListener("click", function() {
      document.getElementById("thirdForm").classList.add("none")



      // /!\/!\ A CONTINUER /!\/!\

      // Appeler la fonction animCbt pour démarrer le combat
      // for (let i = 1; i <= nbTurns; i++) {
        animCbt(perso,MobNotDie,Drake, Potion)
        // if(i != nbTurns){
        //   console.log(`${perso.name} décide de continuer sa route et encore une fois, un monstre se pointe sur sa route !`)
        // }
      // }
      function animCbt(perso,MobNotDie,Drake, Potion){
        // Filtre les Mobs s'ils sont mort ou pas
        MobNotDie = Mobs.filter((e) => e.die === false)
        // Toutes l'animation du combat (avec perso / monstre qui peuvent mourir)
        perso.choisirEtCombattreMonstre(perso, MobNotDie, Drake, Potion);
      }
      document.getElementById("continueLastForm").addEventListener("click", function(){
        document.getElementById("sixthForm").classList.add("none")
        document.getElementById("seventhForm").classList.remove("none")
      })
      // const final = 'Félicitation !'
      // const h1final = document.getElementById('final').textContent = final

    })
  })
// })








