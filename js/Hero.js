import Character from "./Character.js";

export default class Hero extends Character {
    constructor(name,img,health,damage,color,speed, ultimate, passive,die) {
        super(name,img,health,damage,color,speed, die);
        this.ultimate = ultimate;
        this.passive = passive;
    }
    get name() {
        return this._name;
    }
    set name(value) {
        this._name = value;
    }

    get health() {
        return this._health;
    }
    set health(value) {
        this._health = value;
    }
   get damage() {
        return this._damage;
    }
    set damage(value) {
        this._damage = value;
    }
    get color() {
        return this._color;
    }
    set color(value) {
        this._color = value;
    }

    get speed() {
        return this._speed;
    }
    set speed(value) {
        this._speed = value;
    }

    get ultimate() {
        return this._ultimate;
    }
    set ultimate(value) {
        this._ultimate = value;
    }
    get passive() {
        return this._passive;
    }
    set passive(value) {
        this._passive = value;
    }
    get die() {
        return this._die;
    }
    set die(value) {
        this._die = value;
    }
    get img() {
        return this._img;
    }
    set img(value) {
        this._img = value;
    }


     exit() {
        const prevOnError = window.onerror
        window.onerror = () => {
            window.onerror = prevOnError
            return true
        }

        throw new Error(`Fin de la partie`)
    }

    highkick(perso, monstre){
        const highkick = perso.damage * 2;
        monstre.health -= highkick
        return console.log(`[${perso.name} utilise un highkick (${highkick} dmg)] HP: ${perso.health} vs [${monstre.name} defend] HP: ${monstre.health}`);
    }

    tellingPotion(perso, Potion){
        document.getElementById("h3textPotion").innerHTML = "Voulez vous continuer ?"

        // Enlever le 5eme form et faire apparaitre le 6eme form (potion)
        document.getElementById("fifthForm").classList.add("none")
        document.getElementById("sixthForm").classList.remove("none")

        document.getElementById("usePotion").addEventListener("click", function(){
            perso.useHealthPotion(perso,Potion)
        })
    }
    useHealthPotion(perso,potion){
        if(potion.count === 0)
        {
            document.getElementById("h2usePotion").innerHTML = "N'oubliez pas que vous n'avez plus de potion !"
        }
        else{
            const healthBefore = perso._health
            perso._health = (perso._health + 50)
            potion.count = (potion.count - 1)
            document.getElementById("usePotion").innerHTML = `Utiliser une potion (${potion.count})`
            document.getElementById("h2usePotion").innerHTML = `${perso._name} utilise une potion et régénère 50 HP (${healthBefore} -> ${perso._health} HP) ! Plus que ${potion.count} Potions !`
        }

    }

    combatAvecMonstre(perso, monstre, Potion) {
        document.getElementById("fourthForm").classList.add("none")
        document.getElementById("fifthForm").classList.remove("none")
        document.getElementById("h3TextFight").innerHTML = `${perso.name} entre en combat avec ${monstre.name} !`;

        document.getElementById("imgPerso").src = perso.img
        document.getElementById("imgMob").src = monstre.img

        document.getElementById("hpPerso").innerHTML =`HP : ${perso.health}`
        document.getElementById("hpMob").innerHTML = `HP : ${monstre.health}`

        document.getElementById("nomPerso").innerHTML = `${perso.name}`
        document.getElementById("nomMob").innerHTML = `${monstre.name}`



        let tour = 0; // Variable pour suivre le tour

        function attaqueTourParTour(perso, monstre, Potion) {
            document.getElementById("spanButtonTurnByTurn").innerHTML = `Tour ${tour}`
            if (tour % 2 === 0) { // Si le tour est pair, le perso attaque
                let random = Math.floor(Math.random() * 2);
                if(random !== 0){
                    perso.highkick(perso,monstre);

                    document.getElementById("hpPerso").innerHTML =`HP : ${perso.health}`
                    document.getElementById("hpMob").innerHTML = `HP : ${monstre.health}`
                } else{
                    monstre.health -= perso.damage;

                    document.getElementById("hpPerso").innerHTML =`HP : ${perso.health}`
                    document.getElementById("hpMob").innerHTML = `HP : ${monstre.health}`
                }
                if (monstre.health <= 0) {

                    document.getElementById("hpMob").innerHTML = `${monstre.name} est mort`

                    document.getElementById("divButtonTurnByTurnCbt").classList.add("none")
                    document.getElementById("divButtonTurnByTurnCbt").classList.remove("middle")
                    document.getElementById("divButtonToContinue").classList.remove("none")
                    // Fonction qui demande s'il veut utiliser une potion ou pas
                    document.getElementById("buttonToContinue").addEventListener("click", function() {
                        perso.tellingPotion(perso,Potion)
                    })
                    // perso.tellingPotion(perso, Potion)
                    return;
                }
            } else { // Si le tour est impair, le monstre attaque
                perso.health -= monstre.damage;

                document.getElementById("hpPerso").innerHTML =`HP : ${perso.health}`
                document.getElementById("hpMob").innerHTML = `HP : ${monstre.health}`
                if (perso.health <= 0) {
                    document.getElementById("hpPerso").innerHTML = `${perso.name} est mort`
                    console.log("FIN DE LA PARTIE ! (Refresh pour recommencer !)");
                    this.exit();
                    return;
                }
            }
            tour++; // Incrémente le tour
        }

        // Ajoutez un écouteur d'événements au bouton
        document.getElementById("buttonTurnByTurnCbt").addEventListener("click", function() {
            attaqueTourParTour(perso, monstre, Potion);
        });
    }

    choisirEtCombattreMonstre(perso, Mobs, Drake, Potion) {
        // Active le form pour choisir le mob
        document.getElementById("fourthForm").classList.remove("none")


        const selectMobs = document.getElementById("selectMobs")

        for (let i = 0; i < Mobs.length; i++) {
            selectMobs.innerHTML += `<option value="${Mobs[i].name}">${Mobs[i].name}</option>\n`
        }

        selectMobs.addEventListener("change", function(){
            document.getElementById("buttonClassNone").classList.remove("none")
        })

        document.getElementById("buttonChooseMob").addEventListener("click", function() {
            let choix = false;

            const choixMonstre = document.getElementById("selectMobs").value
            let includeMob = false;
            while(includeMob !== true){
                Mobs.forEach(function(mob){
                    if(mob.name === choixMonstre){
                        perso.combatAvecMonstre(perso, mob, Potion);
                        includeMob = true
                    }
                    else if(choixMonstre === 'Drake'){
                        if(Drake.die === false){
                            perso.combatAvecMonstre(perso, Drake, Potion);
                            includeMob = true
                        }else{
                            includeMob = false
                        }
                    }
                })
            }
        })


    }


};