export default class Item {
    constructor(name,count) {
        this._name = name
        this._count = count
    }
    get count() {
        return this._count;
    }

    set count(value) {
        this._count = value;
    }
    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }


}