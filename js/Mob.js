import Character from "./Character.js";

export default class Mob extends Character {
    constructor(name,img,health,damage,color,speed,type,die) {
        super(name,img,health,damage,color,speed,die);
        this.type = type;
    }

    get name() {
        return this._name;
    }
    set name(value) {
        this._name = value;
    }

    get health() {
        return this._health;
    }
    set health(value) {
        this._health = value;
    }
   get damage() {
        return this._damage;
    }
    set damage(value) {
        this._damage = value;
    }
    get color() {
        return this._color;
    }
    set color(value) {
        this._color = value;
    }
    get speed() {
        return this._speed;
    }
    set speed(value) {
        this._speed = value;
    }
    get type() {
        return this._type;
    }
    set type(value) {
        this._type = value;
    }
    get die() {
        return this._die;
    }
    set die(value) {
        this._die = value;
    }
    get img() {
        return this._img;
    }
    set img(value) {
        this._img = value;
    }
};
